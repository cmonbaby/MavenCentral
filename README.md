# MavenCentral

#### 介绍
MavenCentral中央仓库

#### 软件架构
Android Studio上传MavenCentral中央仓库完整源码


#### 安装教程

1.  Android Studio上传MavenCentral中央仓库完整源码
2.  master分支：组合环境（不推荐）
3.  lower分支：AS 3.5.4 + Gradle 5.4.1 + JDK 1.8（推荐）
4.  higher分支：AS 7.0.3 + Gradle 7.0.2 + JDK 11（推荐）

#### 使用说明

1.  只关心module参数配置，动态打包jar/aar
2.  完整性：sources.jar、javadoc.jar、jar / aar、pom
3.  根据版本号动态打包snapshot（快照）/ release（发布）仓库
4.  初始化需要配置本地：local.property（参考local-property）
5.  snapshot快照不需要审核（测试），release发布需要审核（商用）
6.  Nexus提交版本后，Maven同步需要T+1同步

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
