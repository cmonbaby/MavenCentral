package com.simon.maven;

import android.os.Bundle;

import com.cmonbaby.maven.android.IgnoredActivity;
import com.cmonbaby.maven.java.IgnoredClass;

public class MainActivity extends IgnoredActivity {

    private IgnoredClass ignored;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}